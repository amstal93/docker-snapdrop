#!/bin/ash

# replace STUN server
sed -i "s/stun:stun.l.google.com:19302/${STUN_SERVER}/g" client-latest/scripts/network.js

# update client volume
mkdir -p client
mv -f client-latest/* client
rm -rf client-latest

# configure lighttpd
sed -i "s/%HTTP_PORT%/${HTTP_PORT}/g" lighttpd.conf
sed -i "s/%PORT%/${PORT}/g" lighttpd.conf
echo "Proxy hosted on port ${HTTP_PORT}"

# single room mode
if [[ ${SINGLE_ROOM} -eq 1 ]]; then
  echo "Activating single room mode..."
  sed -i 's/peer.ip/0/g' index.js
  sed -i 's/sender.ip/0/g' index.js
fi

# perform audit fixes
npm audit fix

# start server
lighttpd -f lighttpd.conf
NODE_ENV="production" node index.js
